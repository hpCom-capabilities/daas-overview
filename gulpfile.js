var gulp = require('gulp');
var sass = require('gulp-sass');
var webserver = require('gulp-webserver');
var minify = require('gulp-minify');
var concat = require('gulp-concat');
var browserSync = require('browser-sync');
var autoprefixer = require('gulp-autoprefixer');


gulp.task('sass', function () {
  gulp.src('./app/sass/application.scss')
    .pipe(sass())
    .pipe(concat('daas-overview.css'))
    .pipe(autoprefixer({browsers: ['last 4 versions']}))
    .pipe(gulp.dest('./dist/style'));
  gulp.src('./app/sass/fontfaces.scss')
    .pipe(sass())
    .pipe(concat('flex2-fontfaces.css'))
    .pipe(gulp.dest('./dist/style'));
});

gulp.task('js', function() {
  return gulp.src(
    [
      './app/js/jquery-3.2.1.min.js',
      './app/js/slick.min.js',
      './app/js/swiper.jquery.min.js',
      './app/js/metrics.js',
      './app/js/base.js',
      './app/js/youtube.js',
      './app/js/sticky-bar.js',
      './app/js/lifecycle.js'
    ]
  )
    .pipe(concat('daas-overview-min.js'))
    .pipe(gulp.dest('./dist/script/'));
});

gulp.task('html', function() {
  return gulp.src(['./app/*.html'])
    .pipe(gulp.dest('./dist/', { overwhite: true }));
});

gulp.task('compress', function() {
  gulp.src('./dist/script/daas-overview-min.js')
    .pipe(minify({
      ext:{
        src:'.js',
        min:'.min.js'
      }
    }))
    .pipe(gulp.dest('./dist/script/'))
});

gulp.task('serve', ['sass', 'js', 'html', 'compress'], function(){
  gulp.watch('./app/sass/*.scss', ['sass']);
  gulp.watch('./app/js/*.js', ['js']);
  gulp.watch('./app/*.html', ['html']);

  gulp.run('compress');

  gulp.src(['./app/fonts/**/*'])
    .pipe(gulp.dest('./dist/fonts'));

  gulp.src(['./app/images/**/*'])
    .pipe(gulp.dest('./dist/images'));

  gulp.src(['./app/css-images/**/*'])
    .pipe(gulp.dest('./dist/css-images'));

  browserSync({
    notify: false,
    port: 9002,
    startPath: '/',
    server: {
      baseDir: ['dist']
    }
  });
});
