$(function() {

  var isrtlPage = $('html').attr('dir') === 'rtl';

  $('.accordion-head').click(function(){
    $(this).parent('.accordion').toggleClass('active');
  });

  //flag for HP device slider and Customer Stories slider
  var resizeTimeout,
      isHpDevicesSlider = false;
  $(window).on('resize', function () {
    clearTimeout(resizeTimeout);
    resizeTimeout = setTimeout(function () {
      checkSliderInitialization();
    }, 100);
  });

  function checkSliderInitialization() {
    if (window.innerWidth > 1000) {
      if (isHpDevicesSlider) {
        isHpDevicesSlider = false;
        $('.block-slider').slick('unslick');
      }
    } else {
      if (!isHpDevicesSlider) {
        isHpDevicesSlider = true;
        initHpDevicesSlider();
      }
    }
  }
  checkSliderInitialization();

  //HP Devices slider / Customer slider
  function initHpDevicesSlider() {
    $('.block-slider').slick({
      dots: true,
      infinite: false,
      speed: 300,
      arrows: false,
      rtl: isrtlPage
    });
  }

  $('.scroll-to').click(function(e){
    e.preventDefault();
    $('html,body').animate({scrollTop: document.getElementById($(this).attr('data-scroll-to')).offsetTop - 100 + 'px'})
  });
});

