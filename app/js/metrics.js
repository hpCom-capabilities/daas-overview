var metrics = {
  init : function() {


    var meta      = $("meta"),
      _self     = this,
      metaValue = '',
      timer = null,
      selector = 'a, .swiper-pagination-bullet, .swiper-button-prev, .swiper-button-next';

    this.languageCode = document.documentElement.lang.split("-")[0];//ja||us
    this.countryCode =  document.documentElement.lang.split("-")[1];//jp||en

    this.bu = $('[name="bu"]').attr('content');
    this.simple_title = $('[name="simple_title"]').attr('content');

    $(document).on('click custom.invoke', selector, function (e) {

      if (!timer){
        //prevent double event fire
        timer = setTimeout(function () {
          clearTimeout(timer);
          timer = null;
        },100);
        if (e.which !== 3){
          _self.sendMetric( $(this) );
        }
      }
    });

    $(document).on('sendMetric', function (e) {
      _self.sendMetric($(e.target));
    });
  },
  sendMetric: function(elementToTrack){

    var metricsType       = '',
      metricValue       = ""
      metricModuleTitle = $(elementToTrack).closest('.wrap-metric-module').attr('data-metric-module-title'),
      metricLinkTitle = $(elementToTrack).attr('data-metric-title');


    metricValue = (this.bu + '/' + this.countryCode + '/' + this.languageCode + '/' + this.simple_title + '/' +metricModuleTitle+'/' + metricLinkTitle);
    metricsType = elementToTrack.attr('data-metrics-link-type') || '';

    if ( metricsType ) {
      try {
        trackMetrics('newLink', { name :  metricValue, type : metricsType });
      } catch (excpt) {
        console.log("Metrics error:");
        console.log(excpt);
      };
    } else {
      console.log('trackMetrics has NOT beet called cause data-metrics-link-type param is not set to a link');
    }

    return true;
  },
}


$(document).ready(function(){
  metrics.init();
})
