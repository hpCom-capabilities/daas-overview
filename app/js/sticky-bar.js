$(function () {
  var stickyBarControll = {
    initializeBar: function () {
      this.stickySection = $('.sticky-section');
      this.stickyBar = $('.sticky-bar');
    },

    controlStickyPosition: function () {
      var stickySectionTop = this.stickySection.get(0).getBoundingClientRect().top;

      if (stickySectionTop <= 0) {
        this.stickyBar.addClass('fixed');
      } else {
        this.stickyBar.removeClass('fixed');
      }
    },

    checkView: function () {
      var self = this,
          resizeTimeout;
      clearTimeout(resizeTimeout);
      resizeTimeout = setTimeout(function() {
        self.controlStickyPosition();
      }, 200)
    },

    addEvents: function () {
      var self = this;
      this.controlStickyPosition();
      $(window).on('scroll', this.controlStickyPosition.bind(self));
      $(window).on('resize', this.checkView.bind(self))
    },

    init: function () {
      this.initializeBar();
      this.addEvents();
    }
  };

  stickyBarControll.init()
});