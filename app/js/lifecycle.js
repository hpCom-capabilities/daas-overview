$(function () {

  var lifeCycle = {
    chosenElement: 0,
    deegres: 0,
    onePercent: 340 / 100, // value not 360 because we don't have whole circle

    setResult: function (elNum) {
      this.result = this.onePercent * elNum * 16.8; // 16.8 ~ 1/6 part of 100%
    },

    receiveCircleData: function () {
      this.canvas = document.getElementById('circle');
      this.ctx = this.canvas.getContext('2d');
      this.posX = this.canvas.width/2;
      this.posY = this.canvas.height/2;
      this.start = 1.3 * Math.PI;
      this.end = 1.2 * Math.PI;
      this.radius = 175
    },

    createMainCircle: function () {
      this.ctx.beginPath();
      this.ctx.arc( this.posX, this.posY, this.radius, this.start, this.end);
      this.ctx.strokeStyle = '#1159a0';
      this.ctx.lineCap = 'round';
      this.ctx.lineWidth = 5;
      this.ctx.stroke();

      this.createArrows();
    },

    createArrows: function (color) {
      var coord = [
        {from: {x: 75, y: 40}, to: {x: 90, y: 10} }, //left arrow
        {from: {x: 75, y: 40}, to: {x: 105, y: 40} } //right arrow
      ];
      for (var i = 0; i < coord.length; i++) {
        this.ctx.strokeStyle = color || '#1159a0';
        this.ctx.beginPath();
        this.ctx.moveTo(coord[i].from.x,coord[i].from.y);
        this.ctx.lineTo(coord[i].to.x,coord[i].to.y);
        this.ctx.stroke();
      }
    },

    createProgressiveLine: function () {
      this.ctx.beginPath();
      this.ctx.lineWidth = 5;
      this.ctx.strokeStyle = "#00aeef";
      this.ctx.arc(this.posX, this.posY, this.radius, this.end, this.end - (Math.PI/180 * this.deegres), true);
      this.ctx.stroke();
    },

    drawProgressiveLine: function (serviceNum) {
      this.setActiveItem(serviceNum);

      if (serviceNum > this.chosenElement) {
        this.setResult(serviceNum);
        this.increaseProgressLine();
      } else {
        this.setResult(serviceNum);
        this.reduceProgressiveLine();
      }

      this.showContent(serviceNum);
      this.chosenElement = serviceNum;
    },

    increaseProgressLine: function () {
      var self = this;
      var acrInterval = setInterval(function () {
        self.deegres += 3;
        if (self.deegres >= self.result) {
          clearTimeout(acrInterval);
          if (Number(self.chosenElement) === 6) {
            self.createArrows("#00aeef");
          }
          return false;
        }
        self.ctx.clearRect(0, 0, self.canvas.width, self.canvas.height);
        self.createMainCircle();
        self.createProgressiveLine();
      }, 5);
    },

    reduceProgressiveLine: function () {
      var self = this;
      var acrInterval = setInterval(function () {
        self.deegres -= 3;
        if (self.deegres <= self.result) {
          clearTimeout(acrInterval);
          return false;
        }
        self.ctx.clearRect(0, 0, self.canvas.width, self.canvas.height);
        self.createMainCircle();
        self.createProgressiveLine();
      }, 5);
    },

    receiveTriggerButtons: function () {
      this.buttons = $('.lifecycle .service');
    },

    setActiveItem: function (number) {
      this.buttons.removeClass('active');
      $('[data-service="' + number +'"').addClass('active');
    },

    showContent: function (number) {
      var blockNum = number - 1,
          textBlocks = $('.lifecycle .circle-component .text-block');

      textBlocks.eq(blockNum).siblings().hide();
      textBlocks.eq(blockNum).show();
    },

    addEvents: function () {
      var self = this;
      this.buttons.on('click', function () {
        var serviceNum = $(this).attr('data-service');
        self.drawProgressiveLine(serviceNum);
      })
    },

    init: function () {
      this.receiveCircleData();
      this.createMainCircle();
      this.receiveTriggerButtons();
      this.addEvents();
      this.drawProgressiveLine(1);
    }
  };

  lifeCycle.init()
});