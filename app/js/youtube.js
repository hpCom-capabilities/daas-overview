$(function () {
  //Move popups section to the body
  function movePopupsToBody() {
    var popupsSection = $('.popups');
    if (popupsSection.length) {
      $('body').append(popupsSection);
    }
  }
  movePopupsToBody();

  //Video
  $.fn.youtube = function(action) {
    var self = this;
    if (action && $(this).data('initialized')) {
      $(this).trigger(action);
    } else {
      var apiLoaded = new $.Deferred;
      window.onYouTubeIframeAPIReady = function() {
        apiLoaded.resolve();
      };
      var initYoutubeVideo = function(popupEl) {

        var popup = $(popupEl),
          body = $('body'),
          closePopupBtn = popup.find('.close-popup'),
          apiLoaded = new $.Deferred(),
          player = null,
          playerLastState = -1,
          wasPlayed = false,
          states = {
            UNSTARTED: -1,
            ENDED: 0,
            PLAYING: 1,
            PAUSED: 2,
            BUFFERING: 3,
            CUED: 5
          };

        function initVideo() {
          var defered = new $.Deferred;
          var videoBlock = popup.find('.video').attr('id');
          var playerParams = getPlayerParams();
          playerParams.events = createEventsListeners(defered);
          
          new YT.Player(videoBlock, playerParams);
          return defered.promise();
        }
        function getPlayerParams(defered) {
          var htmlLang = $('html').attr('lang');
          var pageLang = htmlLang.length ? htmlLang === 'zh-tw' ? 'zh_tw' :  htmlLang.split('-')[0] : 'en';
          var videoId = popup.attr('data-video-id');
          
          var isPlayList = videoId.indexOf(',') != -1;
          var playerParams = {
            playerVars: {
              rel: 0,
              showinfo: 0,
              controls: 2,
              hl: pageLang,
              cc_lang_pref: pageLang
            } 
          }

          if (isPlayList) {
            playerParams.playerVars.playlist = videoId;
            playerParams.playerVars.showinfo = 1;
          } else {
            playerParams.videoId = videoId;
          }

          return playerParams;
        }
        function createEventsListeners(defered) {
          var showSubtitles = !!body.hasClass('showsubtitles');
          var events = {
            'onReady': function (event) {
              popup.data('initialized', true);
              player = event.target;
              defered.resolve();
            },
            'onStateChange': function (event) {
              metricsOnStateChange(event);
              if (showSubtitles) {
                player.loadModule("captions");
              } else {
                player.unloadModule("captions");
              }
            }
          }

          return events;
        }
        function metricsOnStateChange(event) {
          var videoDurationInSeconds = Math.floor((player.getDuration()-0.01) || 0),
            videoQueuePointInSeconds = Math.round(player.getCurrentTime() || 0),
            videoTitle = player.getVideoData().title;
          try {
            if (playerLastState === states.UNSTARTED ) { //implicit chekcing of unstarted state and calling the open trackvideometrics call
              console.log('** Metrics: YT UNSTARTED **', 'open' + ','+ videoTitle + ',' + videoDurationInSeconds);
              trackVideoMetrics('open', videoTitle, videoDurationInSeconds);
            } else if (event.data === states.PLAYING) { //play state of the video == 1
              wasPlayed = true;
              console.log('** Metrics: YT PLAYING **',  'play' +','+ videoTitle +','+ videoQueuePointInSeconds);
              trackVideoMetrics('play', videoTitle, videoQueuePointInSeconds);
            } else if (event.data === states.PAUSED) { //pause state of the video == 2
              console.log('** Metrics: YT PAUSED **', 'stop' +','+videoTitle +','+ videoQueuePointInSeconds);
              trackVideoMetrics('stop',videoTitle, videoQueuePointInSeconds);
            } else if (event.data === states.ENDED) { //end state of the video == 0
              console.log('** Metrics: YT ENDED **', 'stop' +','+ videoTitle +','+ videoQueuePointInSeconds);
              trackVideoMetrics('close', videoTitle);
            }
          } catch(e) {
            console.log('** Metrics FAILED ** due to: ' + e.message);
          }

          event.data === states.ENDED ? playerLastState = states.UNSTARTED : playerLastState = event.data;
        }
        function showPopup(popup) {
          body.addClass('ovf-hidden');
          popup.show();
          popup.trigger('play');
        }
        function hidePopup(popup) {
          body.removeClass('ovf-hidden');
          popup.trigger('pause');
          popup.hide();
        }
        function addListeners() {
          popup.on('play', function() {
            if (player) {
              if (!window.isMobile || (isTouch && wasPlayed)) {
                player.playVideo();
              }
            } else {
              initVideo().then(function() {
                if (!window.isMobile || (isTouch && wasPlayed)) {
                  player.playVideo();
                }
              });
            }
          }).on('pause', function() {
            player.pauseVideo();
          }).on('show', function() {
            showPopup(popup);
          }).on('hide', function() {
            hidePopup(popup);
          }).on('click', function () {
            closePopupBtn.trigger('click');
          });

          closePopupBtn.on('click', function(e) {
            hidePopup(popup);
            e.preventDefault();
            e.stopPropagation();
            return false;
          });
        }

        addListeners();
        initVideo();
      };
      return $.when(loadScript('https://www.youtube.com/iframe_api'), apiLoaded.promise()).then(function() {
        return $(self).each(function() {
          initYoutubeVideo(this);
        }).data('initialized', true);
      });
    }
  };

  //load any external script
  function loadScript(url) {
    var deffered = new $.Deferred,
      tag = document.createElement('script'),
      firstScriptTag = document.getElementsByTagName('script')[0];

    tag.src = url;
    tag.onload = deffered.resolve;
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    return deffered.promise();
  }

  $('.youtube-popup').youtube();
  $('.show-video').on('click', function() {
    var playerId = $(this).attr('rel');
    $(playerId).youtube('show');
  });
});